var pets = require('./petsFile.js')

console.log(pets)
var express = require('express');

let app = express();

let path = require('path');

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname + '/hello.html'))
})

app.get('/data', (req, res) => {
    res.json(pets)
})

app.get('/pictures', (req, res) => {
    res.sendFile(path.join(__dirname + '/pictures.html'))
})

app.get('/info', (req, res) => {
    res.sendFile(path.join(__dirname + '/info.html'))

})

app.listen(process.env.PORT || 8080)
